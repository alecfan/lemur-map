package io.lemur.map.request.amap.direction;

import static org.junit.Assert.*;
import io.lemur.common.util.json.JSONUtil;
import io.lemur.map.model.amap.bus.AmapBusDirectionModel;
import io.lemur.map.request.spring.SpringTxTestCase;

import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;

public class IAmapDirectionRequestTest extends SpringTxTestCase {

    @Autowired
    private IAmapDirectionRequest amapDirectionRequest;

    @Test
    public void testBusDirection() {
        AmapBusDirectionModel model = amapDirectionRequest.busDirection("0731", "0", "113.006661,28.200204",
            "112.91931,28.21386", "90c9e3b1530ae18fcbe32809f4b8823e");
        System.out.println(JSONUtil.toJson(model));
    }

}
