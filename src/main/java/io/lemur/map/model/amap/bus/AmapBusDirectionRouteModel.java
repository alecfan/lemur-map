package io.lemur.map.model.amap.bus;

import java.util.List;

import com.fasterxml.jackson.annotation.JsonProperty;

/**   
 * @Description:  
 * @author Lemur
 * @date 2015-01-29 13:01
 * @version V1.0   
 */
public class AmapBusDirectionRouteModel implements java.io.Serializable {

    private static final long                   serialVersionUID = 1L;
    /**
     *  
     **/
    private String                              origin;
    /**
     *  
     **/
    private String                              destination;
    /**
     *  
     **/
    private String                              distance;
    /**
     *  
     **/
    @JsonProperty("taxi_cost")
    private String                              taxiCost;
    /**
     *  
     **/
    private List<AmapBusDirectionTransitsModel> transits;

    /**
     *方法: 取得
     *@return: String  
     */
    public String getOrigin() {
        return this.origin;
    }

    /**
     *方法: 设置
     *@param: origin  
     */
    public void setOrigin(String origin) {
        this.origin = origin;
    }

    /**
     *方法: 取得
     *@return: String  
     */
    public String getDestination() {
        return this.destination;
    }

    /**
     *方法: 设置
     *@param: destination  
     */
    public void setDestination(String destination) {
        this.destination = destination;
    }

    /**
     *方法: 取得
     *@return: String  
     */
    public String getDistance() {
        return this.distance;
    }

    /**
     *方法: 设置
     *@param: distance  
     */
    public void setDistance(String distance) {
        this.distance = distance;
    }

    /**
     *方法: 取得
     *@return: String  
     */
    public String getTaxiCost() {
        return this.taxiCost;
    }

    /**
     *方法: 设置
     *@param: taxiCost  
     */
    public void setTaxiCost(String taxiCost) {
        this.taxiCost = taxiCost;
    }

    /**
     *方法: 取得
     *@return: List<AmapBusDirectionTransitsModel>  
     */
    public List<AmapBusDirectionTransitsModel> getTransits() {
        return this.transits;
    }

    /**
     *方法: 设置
     *@param: transits  
     */
    public void setTransits(List<AmapBusDirectionTransitsModel> transits) {
        this.transits = transits;
    }
}
