package io.lemur.map.model.amap.bus;

/**   
 * @Description:  
 * @author Lemur
 * @date 2015-01-29 13:01
 * @version V1.0   
 */
public class AmapBusDirectionSegmentsModel implements java.io.Serializable {

    private static final long            serialVersionUID = 1L;
    /**
     *  
     **/
    private AmapBusDirectionWalkingModel walking;
    /**
     *  
     **/
    private AmapBusDirectionBusModel     bus;
    /**
     *方法: 取得
     *@return: AmapBusDirectionWalkingModel  
     */
    public AmapBusDirectionWalkingModel getWalking() {
        return this.walking;
    }

    /**
     *方法: 设置
     *@param: walking  
     */
    public void setWalking(AmapBusDirectionWalkingModel walking) {
        this.walking = walking;
    }

    /**
     *方法: 取得
     *@return: AmapBusDirectionBusModel  
     */
    public AmapBusDirectionBusModel getBus() {
        return this.bus;
    }

    /**
     *方法: 设置
     *@param: bus  
     */
    public void setBus(AmapBusDirectionBusModel bus) {
        this.bus = bus;
    }

}
