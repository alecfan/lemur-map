package io.lemur.map.request.amap.direction;

import io.lemur.http.annotation.IRequest;
import io.lemur.http.annotation.IRequestMethod;
import io.lemur.http.annotation.IRequestParam;
import io.lemur.http.entity.enums.RequestTypeEnum;
import io.lemur.map.model.amap.bus.AmapBusDirectionModel;

/**
 * 
 * @author JueYue
 * @date 2015年1月31日
 */
@IRequest("amapDirectionRequest")
public interface IAmapDirectionRequest {

    @IRequestMethod(type = RequestTypeEnum.GET, url = "http://restapi.amap.com/v3/direction/transit/integrated?extensions=all&s=rsv3&rf=h5&utm_source=litemap")
    public AmapBusDirectionModel busDirection(@IRequestParam("city") String city,
                                              @IRequestParam("strategy") String strategy,
                                              @IRequestParam("origin") String origin,
                                              @IRequestParam("destination") String destination,
                                              @IRequestParam("key") String key);

}
